import os

from PyQt4.QtGui import QWidget, QVBoxLayout, QHBoxLayout, QPushButton, QFileDialog, QLabel, QApplication
from PyQt4.QtCore import QSettings, QTranslator
from PyQt4.QtGui import QApplication

import logging
import shutil

# -*- coding: utf-8 -*-
import sys
import sip

reload(sys)
sys.setdefaultencoding('utf8')
sys.path.append("./")

# sip.setapi('QString', 2)
# sip.setapi('QVariant', 2)
# sip.setapi('QDate', 2)
# sip.setapi('QDateTime', 2)
# sip.setapi('QTime', 2)
# sip.setapi('QTextStream', 2)
# sip.setapi('QUrl', 2)
#
# os.environ['ETS_TOOLKIT'] = 'qt4'
# os.environ['QT_API'] = 'pyqt'
# os.environ['ETS_TOOLKIT'] = 'qt4'
# os.environ['QT_API'] = 'pyqt'

logging.basicConfig(level=logging.DEBUG)

from UI.mainWindow import MainWindow
from shared.bindModel import BindModel

def main():
    app = QApplication(sys.argv)
    #app.setStyle('CleanLooks')
    app.setStyle('Fusion')

    app.processEvents()

    model = BindModel()
    mainWindow = MainWindow(model)
    mainWindow.showMaximized()


    sys.exit(app.exec_())

main()