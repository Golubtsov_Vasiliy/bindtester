# -*- coding: utf-8 -*-

from PyQt4 import QtCore

import os
from PyQt4.QtCore import QFileInfo, QEvent
from PyQt4.QtGui import QMainWindow, QVBoxLayout, QWidget
from UI.videoViewer import VideoPlayer
from UI.bindControls import BindControls

class MainWindow(QMainWindow):
    def __init__(self, model):
        super(MainWindow, self).__init__()

        self.model = model

        self.initUI()

    def __del__(self):
        pass

    def initUI(self):
        centralWidget = QWidget()

        mainLayout = QVBoxLayout()
        centralWidget.setLayout(mainLayout)
        self.setCentralWidget(centralWidget)

        self.controls = BindControls()

        self.videoPlayer = VideoPlayer()
        mainLayout.addWidget(self.videoPlayer)
        mainLayout.addWidget(self.controls)

        self.videoPlayer.show()
        self.show()
