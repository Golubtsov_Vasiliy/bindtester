from PyQt4.QtCore import QDir, Qt, QUrl
from PyQt4.QtGui import QHBoxLayout, QWidget, QPushButton

class BindControls(QWidget):
    def __init__(self):
        super(BindControls, self).__init__()


        self.initUI()
        pass

    def initUI(self):
        startPause = QPushButton()

        controlsLayout = QHBoxLayout()
        self.setLayout(controlsLayout)

        startPause.setText(">")
        controlsLayout.addWidget(startPause)