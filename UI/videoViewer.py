from PyQt4.QtCore import QDir, Qt, QUrl
from PyQt4.QtGui import QVBoxLayout, QWidget
from PyQt4.phonon import Phonon

class VideoPlayer(QWidget):
    def __init__(self):
        super(VideoPlayer, self).__init__()
        videoLayout = QVBoxLayout()
        self.setLayout(videoLayout)

        self.videoPlayer = Phonon.VideoPlayer()
        self.videoLayout.addWidget(self.videoPlayer)
        self.videoPlayer.tick.connect(self._tick)

        self.curSourceName = ""

        self.beginTick = 0
        self.endTick = 0

    def loadBind(self, bind):
        if self.curSourceName != bind.sourceVideo:
            self.curSourceName = bind.sourceVideo
            media = Phonon.MediaSource(self.curSourceName)
            self.videoPlayer.load(media)

        self._playPart(bind.soundBegin, bind.soundEnd)

    def _playPart(self, beginTime, endTime):
        self.beginTick = self._timeToTick(beginTime)
        self.endTick = self._timeToTick(endTime)

        self.videoPlayer.seek(self.beginTick)
        self.videoPlayer.play()

    def _timeToTick(self, time):

        return 0

    def _tick(self, tick):
        if tick > self.endTick:
            self.videoPlayer.pause()